package com.ideal.notification.study;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "notification";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void notification(View view) {

        Intent intent = new Intent(this,NotificationActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = TaskStackBuilder.create(this)
                .addNextIntentWithParentStack(intent)
                .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent snoozeIntent = new  Intent(this,NotificationReceiver.class );

        PendingIntent snoozePendingIntent = PendingIntent.getBroadcast(this, 0, snoozeIntent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            Notification notification = new Notification.Builder(this, "hhh")
                    .setContentTitle("8.0+标题")
                    .setContentText("8.0+内容")
                    .setContentIntent(pendingIntent)
                    // .setDefaults(DEFAULT_ALL)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .addAction(new Notification.Action.Builder(Icon.createWithResource(getPackageName(),
                            R.mipmap.ic_launcher_round), "snooze1", snoozePendingIntent).build())
                    .addAction(new Notification.Action.Builder(Icon.createWithResource(getPackageName(),
                            R.mipmap.ic_launcher_round), "snooze2", snoozePendingIntent).build())
                    .addAction(new Notification.Action.Builder(Icon.createWithResource(getPackageName(),
                            R.mipmap.ic_launcher_round), "snooze3", snoozePendingIntent).build())
                    .build();

            NotificationChannel channel = new NotificationChannel("hhh","ideal",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);


            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            if (null!=notificationManager) {

                notificationManager.createNotificationChannel(channel);

                notificationManager.notify(100, notification);
            }


        }else {
            NotificationCompat.Builder builder;

            builder = new NotificationCompat.Builder(this,"hhh");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setPriority(NotificationManager.IMPORTANCE_MAX);

                RemoteInput remoteInput = new RemoteInput.Builder("resultKey").build();
            }
            Notification   notification = builder
                    .setContentTitle("标题")
                    .setContentText("内容")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .addAction(new NotificationCompat.Action.Builder(
                            R.mipmap.ic_launcher_round, "snooze1", snoozePendingIntent
                    ).build())
                    .addAction(new NotificationCompat.Action.Builder(
                            R.mipmap.ic_launcher_round, "snooze2", snoozePendingIntent
                    ).build()).addAction(new NotificationCompat.Action.Builder(
                            R.mipmap.ic_launcher_round, "snooze3", snoozePendingIntent
                    ).build())
                    .build();


            NotificationManagerCompat.from(this).notify(1,notification);

        }





    }
}
